The source code and accompanying files are provided under the Apache v2 license (or respective license for prior work).

This framework provides WebSocket connectivity to a Burrow Blockchain (Monax.io) in Swift.

It generates a unique id (sequential) per request, and also handles sequence numbers for transactions.

The framework implements this:
https://github.com/hyperledger/burrow/blob/master/docs/specs/api.md

The standard request are:
- BlockchainRequest: provides capabilities to call the block related functions
- EventRequest: provides event listening capabilities
- CallRequest: provides capabilities to call const methods on contracts
- TransactionRequest: provides capabilities to send signed transactions to the blockchain
- ConsensusRequest: allows querying of the consensus
- NetworkRequest: allows querying of the state of the network
- StorageRequest: allows querying storage at a specific address
- NameRegistryRequest: allows querying the name registry (and creating new names)

Additional requests are also available:
- NewAccountRequest: if the BurrowBlockchain object has been instantiated with a key for an account with createAccount and set_base permissions, this framework can create new users with the relevant permissions.

On Linux, it uses the Coroutine based Zewo framework (zewo.io).

On MacOS, it uses by default the Coroutine based Zewo framework, but can also use traditional macOS multithreading. In that case, one needs to use Starscream websockets instead of Zewo websockets. At this time, it requires changes to Package.swift and Sources/BurrowBlockchain.swift.

On iOS, it uses Starscream websockets (github.com/daltoniam/Starscream). 

Please note that we use our own branch of Starscream due to a bug with Foundation.Data used in handling the socket stream. 



Version: 0.5.1

Usage: In a standard Swift Package Manager directory tree:
in Package.swift:
```swift
import PackageDescription

let package = Package(
    name: "BurrowApp",
dependencies: [ .Package(url: "git@gitlab.com:katalysis/ErisKeys.git", majorVersion: 0, minor: 3),
dependencies: [ .Package(url: "git@gitlab.com:katalysis/BurrowBlockchain.git", majorVersion: 0, minor: 5),
]
)
```

in Sources/main.swift
```swift
import Foundation
import ErisKeys
import BurrowBlockchain

// Private seed for an account on the blockchain ([UInt8] of length 32). 
let seed: [UInt8] = [0x19,0x8C,0x02,0xC9,0xE2,0xA9,0x38,0xE8,0x55,0xF8,0x25,0xB3,0xB0,0xDB,0x06,0xD5,0xD8,0xA1,0xC5,0x2A,0xE4,0xB6,0xA2,0x93,0x4B,0x50,0xDC,0xFB,0xB0,0x89,0xE7,0x99]

let ek = ErisKey(seed: seed)

// Blockchain websocket endpoint (ws or wss protocol)
let blockchainURL = URL(string: "ws://ebook1.katalysis.nl:8081")!

let bc = BurrowBlockchain(url: url, key: ek, "Test", true, true)

bc.initialize()
// at that stage, the bc object will send two messages to the blockchain:
// 1. get the chain Id
// 2. get the account tied to the provided key

// if the key has no valid account on the chain, it will return with various values set to 0, such as its balance, its permissions.

```

Supported toolchain:
3.1.1 (Xcode 8.3.3)

Supported plaftorms:
- macOS
- Linux
- iOS (Podfile only)

```
source 'https://github.com/CocoaPods/Specs.git'
source 'https://gitlab.com/katalysis/Pods.git'

target 'BurrowApp' do
  # Comment the next line if you're not using Swift and don't want to use dynamic frameworks
  use_frameworks!

  pod 'BurrowBlockchain', '~> 0.5.1'

  target 'BurrowAppTests' do
    inherit! :search_paths
    # Pods for testing
  end

end

```

If you find this library useful, please donate at: https://dweller.katalysis.io
. Many thanks!
