import XCTest
@testable import BurrowBlockchainTests

XCTMain([
    testCase(BurrowBlockchainTests.allTests),
])
