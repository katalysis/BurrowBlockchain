//
//  CallRequest.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 30/09/2016.
//  Copyright © 2016-17 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

import Foundation
import JSONCodable
// Code execution

public class CallRequest: BurrowMessage {
  public let method = "\(BURROWDB).call"
  public let params: CallParams

  public typealias Response = CallResponse
  public init(id: RequestId, from: Address, to: Address, data: BCData, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    self.params = CallParams(from: from, to: to, data: data)
    super.init(id:id, callback: callback)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    params = try decoder.decode("params")
    try super.init(object:object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
      try encoder.encode(params, key: "params")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }

  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
}

public class CallResponse: BurrowResponse {
  public let result: CallResult

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}

public struct CallParams: JSONCodable {
  public let from: Address
  public let to: Address
  public let data: BCData

  public init(from: Address, to: Address, data: BCData) {
    self.from = from
    self.to = to
    self.data = data
  }

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    from = try decoder.decode("from")
    to = try decoder.decode("address")
    data = try decoder.decode("data")
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(from, key: "from")
      try encoder.encode(to, key: "address")
      try encoder.encode(data, key: "data")
    })
  }
}

public struct CallResult: JSONCodable {
  public let returnV: String
  public let gas_used: Int

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    returnV = try decoder.decode("return")
    gas_used = try decoder.decode("gas_used")
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(returnV, key: "return")
      try encoder.encode(gas_used, key: "gas_used")
    })
  }
}
