//
//  TransactionRequest.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 30/09/2016.
//  Copyright © 2016-17 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

import Foundation
import ErisKeys
import JSONCodable

// Transactions

public class BroadcastTxRequest: BurrowMessage {
  public let method = "\(BURROWDB).broadcastTx"
  public let params: Tx
  
  public typealias Response = BroadcastTxResponse
  public init(id: RequestId, tx: Tx, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    self.params = tx
    super.init(id:id, callback: callback)
  }
  
  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    params = try decoder.decode("params")
    try super.init(object:object)
  }
  
  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
    })
    // [type, Tx]
    var t = [Any]()
    t.append(try Int(params.type.rawValue).toJSON())
    t.append(try params.toJSON())
    res["params"] = t
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
  
  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
  
    public override func sign(_ sequence: (Void) -> (Int), _ chainId: (Void) -> (String), _ key: ErisKey) {
    let seq = sequence()
    let chId = chainId()
    params.sign(seq, chId, key)
  }
}

public class BroadcastTxAndHoldRequest: BurrowMessage {
    public let method = "\(BURROWDB).broadcastTxAndHold"
    public let params: Tx
    
    public typealias Response = EventCallResponse
    public init(id: RequestId, tx: Tx, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
        self.params = tx
        super.init(id:id, callback: callback)
    }
    
    public required init(object: JSONObject) throws {
        let decoder = JSONDecoder(object: object)
        params = try decoder.decode("params")
        try super.init(object:object)
    }
    
    public override func toJSON() throws -> Any {
        var res = try JSONEncoder.create({ (encoder) in
            try encoder.encode(method, key: "method")
        })
        // [type, Tx]
        var t = [Any]()
        t.append(try Int(params.type.rawValue).toJSON())
        t.append(try params.toJSON())
        res["params"] = t
        for e in (try super.toJSON()) as! JSONObject {
            res[e.key] = e.value
        }
        return res
    }
    
    public override func parseResponse(JSONString: String) throws -> BurrowResponse {
        return callback(try Response(JSONString: JSONString))
    }
    
    public override func sign(_ sequence: (Void) -> (Int), _ chainId: (Void) -> (String), _ key: ErisKey) {
        let seq = sequence()
        let chId = chainId()
        params.sign(seq, chId, key)
    }
}


public class BroadcastTxResponse: BurrowResponse {
  public let result: BroadcastTxResult
  
  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }
  
  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}

public struct BroadcastTxResult: JSONCodable {
  public let tx_hash: TxHash
  public let creates_contract: Bool
  public let contract_addr: Address
  
  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    tx_hash = try decoder.decode("tx_hash")
    do {
      creates_contract = try decoder.decode("creates_contract")
    } catch {
      let i: Int = try decoder.decode("creates_contract")
      if (i == 0) {
        creates_contract = false
      } else {
        creates_contract = true
      }
    }
    contract_addr = try decoder.decode("contract_addr")
  }
  
  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(tx_hash, key: "tx_hash")
      try encoder.encode(creates_contract, key: "creates_contract")
      try encoder.encode(contract_addr, key: "contract_addr")
    })
  }
}



public class GetUnconfirmedTxsRequest: BurrowMessage {
  public let method = "\(BURROWDB).getUnconfirmedTxs"
  public typealias Response = GetUnconfirmedTxsResponse
  
  public override init(id: RequestId) {
    super.init(id:id)
  }
  
  public required init(object: JSONObject) throws {
    try super.init(object:object)
  }
  
  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
  
  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return try Response(JSONString: JSONString)
  }
  
}

public typealias GetUnconfirmedTxsResult = Block

public class GetUnconfirmedTxsResponse: BurrowResponse {
  public let result: GetUnconfirmedTxsResult
  
  
  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }
  
  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}
