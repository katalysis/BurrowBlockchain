//
//  BurrowMessage.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 11/07/16.
//  Copyright © 2016-17 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

import Foundation
import ErisKeys
import JSONCodable

/*
 extension JSONDecoder {
 public func decode(_ key: String) throws -> Date {
 return try decode(key, transformer: JSONTransformer.stringToDate)
 }
 }
 */

public let JSON_RPC = "2.0"
public let BURROWDB = "burrow"


public enum RPCCallDecodeError: Error {
  case wrongJSONVersion(expected: String, got: String)
  case wrongMethod(expected: String, got: String)
}

extension RPCCallDecodeError: CustomStringConvertible {
  public var description: String {
    switch self {
    case .wrongJSONVersion(let expected, let got):
      return String("Wrong JSON Version. Expected: \(expected), got: \(got)")
    case .wrongMethod(let expected, let got):
      return String("Wrong Method. Expected: \(expected), got: \(got)")
    }
  }
}


public typealias Address = String // 20 bytes as hex string
public typealias BCData = String // array of bytes as hex string
extension UInt: JSONCompatible {}
extension UInt64: JSONCompatible {}

public enum KeyType: Int {
    case ed25519 = 1
    case secpk261 = 2
}

public struct PubKey {
    public let type: KeyType
    public let key: String // 32 bytes hex string
    
    public init(key: String, type: KeyType = .ed25519) {
        self.type = type
        self.key = key
    }
}


public struct PermFlag: OptionSet {
    public let rawValue: Int
    
    public init(rawValue: Int) {
        self.rawValue = rawValue
    }
    
    // chain permissions
    public static let root = PermFlag(rawValue: 1 << 0)
    public static let send = PermFlag(rawValue: 1 << 1)
    public static let call = PermFlag(rawValue: 1 << 2)
    public static let createContract = PermFlag(rawValue: 1 << 3)
    public static let createAccount = PermFlag(rawValue: 1 << 4)
    public static let bond = PermFlag(rawValue: 1 << 5)
    public static let name = PermFlag(rawValue: 1 << 6)
    // moderator permissions
    public static let hasBase = PermFlag(rawValue: 1 << 7)
    public static let setBase = PermFlag(rawValue: 1 << 8)
    public static let unsetBase = PermFlag(rawValue: 1 << 9)
    public static let setGlobal = PermFlag(rawValue: 1 << 10)
    public static let hasRole = PermFlag(rawValue: 1 << 11)
    public static let addRole = PermFlag(rawValue: 1 << 12)
    public static let rmRole = PermFlag(rawValue: 1 << 13)
}

public struct BurrowError: JSONCodable {  public let code: BurrowRPCErrorCode
  public let message: String
  
  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    code = try decoder.decode("code")
    message = try decoder.decode("message")
  }
  
  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(code, key: "code")
      try encoder.encode(message, key: "message")
    })
  }
}


public enum BurrowRPCErrorCode: Int {
  case PARSE_ERROR      = -32700
  case INVALID_REQUEST  = -32600
  case METHOD_NOT_FOUND = -32601
  case INVALID_PARAMS   = -32602
  case INTERNAL_ERROR   = -32603
}



public typealias RequestId = String

public struct RequestIdGenerator { // TODO: make thread safe
  static var num = UInt(0)
  let prefix: String
  
  init(_ prefix: String)
  {
    self.prefix = prefix
  }
  
  public func next() -> RequestId {
    let nxt = prefix + "/" + String(RequestIdGenerator.num)
    // TODO: claim lock
    RequestIdGenerator.num += 1
    // TODO: release lock
    return RequestId(nxt)
  }
}

open class BurrowMessage: JSONCodable {
  public let id: RequestId
  public var callback: (BurrowResponse) -> BurrowResponse
  
  public typealias Response = BurrowResponse
  
  public init(id: RequestId) {
    self.id = id
    self.callback = {r in return r}
  }
  
  public init(id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    self.id = id
    self.callback = callback
  }
  
  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    let jsonrpc:String = try decoder.decode("jsonrpc")
    if (jsonrpc != JSON_RPC) {
      throw RPCCallDecodeError.wrongJSONVersion(expected: JSON_RPC, got: jsonrpc)
    }
    id = try decoder.decode("id")
    self.callback =  {r in return r}
  }
  
  open func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(JSON_RPC, key: "jsonrpc")
      try encoder.encode(id, key: "id")
    })
  }
  
  open func parseResponse(JSONString:String) throws -> BurrowResponse {
    return try Response(JSONString: JSONString)
  }
  
  open func sign(_ sequence: (Void) -> (Int),_ chainId: (Void) -> (String), _ key:ErisKey) {}
  // using a function allows to delay argument evaluation
}

open  class BurrowResponse: BurrowMessage {
  public let error: BurrowError?
  
  public init(id: RequestId, error: BurrowError? = nil) {
    self.error = error
    super.init(id: id)
  }
  
  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    var err: BurrowError?
    do {
      err = try decoder.decode("error")
    } catch {
      err = nil
    }
    error = err
    try super.init(object: object)
  }
  
  open override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(error, key: "error")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}


public struct FilterData: JSONCodable {
  public let field: String
  public let op: String
  public let value: String
  
  public init(field: String, op: String, value: String) {
    self.field = field
    self.op = op
    self.value = value
  }
  
  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    field = try decoder.decode("field")
    op = try decoder.decode("op")
    value = try decoder.decode("value")
  }
  
  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(field, key: "field")
      try encoder.encode(op, key: "op")
      try encoder.encode(value, key: "value")
    })
  }
}

public struct FiltersParams: JSONCodable {
  public let filters: [FilterData] // balance or code
  
  public init(_ filters: [FilterData]) {
    self.filters = filters
  }
  
  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    filters = try decoder.decode("filters")
  }
  
  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(filters, key: "filters")
    })
  }
}
