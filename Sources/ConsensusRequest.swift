//
//  ConsensusRequest.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 30/09/2016.
//  Copyright © 2016-17 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

import Foundation
import JSONCodable

// TODO: there seems to be a call missing from WS RPC (dump_consensus_state). check on Eris Go code

// Consensus
public class GetConsensusStateRequest: BurrowMessage {

  public let method = "\(BURROWDB).getConsensusState"

  public typealias Response = GetConsensusStateResponse

  public override init(id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    super.init(id:id, callback: callback)
  }

  public required init(object: JSONObject) throws {
    try super.init(object:object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }

  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
}


public class GetConsensusStateResponse: BurrowResponse {
  public let result: ConsensusResult

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}

public struct ConsensusResult: JSONCodable {
  public let height: BlockHeight
  public let round: Int
  public let step: Int
  public let start_time: Date?
  public let commit_time: Date?
  public let validators:  [Validator]
  public let proposal: Proposal?

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    height = try decoder.decode("height")
    round = try decoder.decode("round")
    step = try decoder.decode("step")
    do {
      start_time = try decoder.decode("start_time", transformer: JSONTransformers.StringToDate)
    } catch {
      start_time = nil //TODO: fix (likely in Eris). format has 8 digits for ms "2016-10-04 20:47:24.83314181 +0000 UTC"
    }
    do {
      commit_time = try decoder.decode("commit_time", transformer: JSONTransformers.StringToDate)
    } catch {
      commit_time = nil  //TODO: fix (likely in Eris). format is wrong "0001-01-01 00:00:00 +0000 UTC"
    }
    validators = try decoder.decode("validators")
    do {
      proposal = try decoder.decode("proposal")
    } catch {
      proposal = nil
    }
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(height, key: "height")
      try encoder.encode(round, key: "round")
      try encoder.encode(step, key: "step")
      try encoder.encode(start_time, key: "start_time", transformer: JSONTransformers.StringToDate)
      try encoder.encode(commit_time, key: "commit_time", transformer: JSONTransformers.StringToDate)
      try encoder.encode(validators, key: "validators")
      try encoder.encode(proposal, key: "proposal")
    })
  }
}

public struct Proposal: JSONCodable {
  public let height: BlockHeight
  public let round: Int
  public let block_parts_header:  BlockPart
  public let pol_round: Int
  public let signature: Signature

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    height = try decoder.decode("height")
    round = try decoder.decode("round")
    block_parts_header = try decoder.decode("block_parts_header")
    pol_round = try decoder.decode("pol_round")
    let sig: String = try decoder.decode("signature[1]")
    let type: KeyType = try decoder.decode("signature[0]")
    signature = Signature(sig,type: type)
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(height, key: "height")
      try encoder.encode(round, key: "round")
      try encoder.encode(block_parts_header, key: "block_parts_header")
      try encoder.encode(pol_round, key: "pol_round")
      try encoder.encode(signature.sig, key: "signature[1")
      try encoder.encode(signature.type, key: "signature[0]")
    })
  }
}

public struct Validator: JSONCodable {
  public let address: Address
  public let pub_key: PubKey
  public let bond_height: BlockHeight
  public let unbond_height: BlockHeight
  public let last_commit_height: BlockHeight
  public let voting_power: Int
  public let accum: Int

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    address = try decoder.decode("address")
    let key: String = try decoder.decode("pub_key[1]")
    let type: KeyType = try decoder.decode("pub_key[0]")
    pub_key = PubKey(key: key,type: type)
    bond_height = try decoder.decode("bond_height")
    unbond_height = try decoder.decode("unbond_height")
    last_commit_height = try decoder.decode("last_commit_height")
    voting_power = try decoder.decode("voting_power")
    accum = try decoder.decode("accum")
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(address, key: "address")
      try encoder.encode(pub_key.key, key: "pub_key[1")
      try encoder.encode(pub_key.type, key: "pub_key[0]")
      try encoder.encode(bond_height, key: "bond_height")
      try encoder.encode(unbond_height, key: "unbond_height")
      try encoder.encode(last_commit_height, key: "last_commit_height")
      try encoder.encode(voting_power, key: "voting_power")
      try encoder.encode(accum, key: "accum")
    })
  }
}

public class GetValidatorsRequest: BurrowMessage {

  public let method = "\(BURROWDB).getValidators"
  public typealias Response = GetValidatorsResponse

  public override init(id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    super.init(id:id, callback: callback)
  }

  public required init(object: JSONObject) throws {
    try super.init(object:object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }

  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
}

public class GetValidatorsResponse: BurrowResponse {

  public let result: ValidatorsResult

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}

public struct ValidatorsResult: JSONCodable {
  public let block_height: BlockHeight
  public let bonded_validators: [Validator]
  public let unbonding_validators: [Validator]

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    block_height = try decoder.decode("block_height")
    bonded_validators = try decoder.decode("bonded_validators")
    unbonding_validators = try decoder.decode("unbonding_validators")
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(block_height, key: "block_height")
      try encoder.encode(bonded_validators, key: "bonded_validators")
      try encoder.encode(unbonding_validators, key: "unbonding_validators")
    })
  }
}
