//
//  NewAccountRequest.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 24/10/2016.
//  Copyright © 2016-17 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//


// TODO: Strictly speaking, this is not an Eris message. needs to be moved out properly.
// maybe have a mehtod and a service, where method will be what is after the . and service
// is BURROWDB, account,...

import Foundation
import JSONCodable

public class NewAccountRequest: BurrowMessage {
  public let method = "account.new"
  public let account: Address

  public typealias Response = NewAccountResponse
  public init(id: RequestId, account: Address, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    self.account = account
    super.init(id:id, callback: callback)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    account = try decoder.decode("account")
    try super.init(object:object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
      try encoder.encode(account, key: "account")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }

  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
}

public class NewAccountResponse: BurrowResponse {
  public let result: Bool

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}
