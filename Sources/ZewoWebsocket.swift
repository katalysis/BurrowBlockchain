//
//  ZewoWebsocket.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 08/10/2016.
//  Copyright © 2016-17 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

#if os(Linux) || os(OSX)
  import Foundation
  import WebSocketClient
  import Venice
  
  
  public class ZewoWebSocket: WebSocketWrapper {
    var wsock : WebSocket? = nil
    var client: WebSocketClient? = nil
    var url: URL
    var connected: Bool = false
    public var onConnect: ((Void) -> Void)?
    public var onDisconnect: ((NSError?) -> Void)?
    public var onText: ((String) -> Void)?
    public var onData: ((Data) -> Void)?
    public var onPong: ((Data?) -> Void)?
    
    public var isConnected: Bool {
      return connected
    }
    
    public init(url: URL = URL(string: "ws://127.0.0.1:8081")!) {
      self.url = url
    }
    
    public func write(string: String, completion: (() -> ())?) {
      do {
        try wsock!.send(string)
      } catch let error {
        print(error)
      }
    }
    
    public func write(ping: Data, completion: (() -> ())?) {
      do {
        try wsock!.ping()
      } catch let error {
        print(error)
      }
    }
    
    public func connect() {
      do {
        
        client = try WebSocketClient(url: url,
                                     didConnect: { ws in
                                      self.connected = true
                                      self.onConnect!()
                                      ws.onBinary { data in
                                        self.onData!(Data(data))
                                      }
                                      ws.onText { text in
                                        self.onText!(text)
                                      }
                                      ws.onPing { (data) in
                                        self.onPong!(Data(data))
                                      }
                                      ws.onPong { (data) in
                                        self.onPong!(Data(data))
                                      }
                                      ws.onClose {(code, reason) in
                                        self.connected = false
                                        self.onDisconnect!(NSError(domain: "\(String(describing: code)):\(String(describing: reason))", code: 0 , userInfo: nil))
                                        
                                      }
                                      self.wsock = ws
        })
        
        
        client!.connectInBackground()
        
      } catch let error {
        print("Error Occured \(error)")
      }
    }
    
    public func listenForResponse(isTrue: () -> Bool, timeout: Double, process: (() -> Void)? = nil){
      var done = false
      let timer = Timer(deadline: timeout.seconds.fromNow())
      co {
        timer.channel.receive()
        done = true
      }
      while (isTrue() && !done) {
        if (process != nil) {
          process!()
        }
        nap(for: 0.01)
      }
    }
    
    
  }
  
#endif


