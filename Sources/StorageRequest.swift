//
//  StorageRequest.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 30/09/2016.
//  Copyright © 2016-17 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

import Foundation
import JSONCodable

// Storage

public class GetStorageRequest: BurrowMessage {
  public let method = "\(BURROWDB).getStorage"
  public let params: Address  // TODO: can be absorbed

  public typealias Response = GetStorageResponse
  public init(id: RequestId, address: Address, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    self.params = address
    super.init(id:id, callback: callback)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    params = try decoder.decode("params")
    try super.init(object:object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
      try encoder.encode(params, key: "params")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }

  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
}

public class GetStorageResponse: BurrowResponse {
  public let result: Storage

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}

public struct Storage: JSONCodable {
  public let storage_root: Address
  public let storage_items: [StorageItem]

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    storage_root = try decoder.decode("storage_root")
    storage_items = try decoder.decode("storage_items")
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(storage_root, key: "storage_root")
      try encoder.encode(storage_items, key: "storage_items")
    })
  }
}

public struct StorageItem: JSONCodable {
  public let key: String
  public let value: String

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    key = try decoder.decode("key")
    value = try decoder.decode("value")
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(key, key: "key")
      try encoder.encode(value, key: "value")
    })
  }
}

public class GetStorageAtRequest: BurrowMessage {

  public let method = "\(BURROWDB).getStorageAt"
  public let params: StorageAt

  public typealias Response = GetStorageAtResponse
  public init(id: RequestId, params: StorageAt, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    self.params = params
    super.init(id:id, callback: callback)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    params = try decoder.decode("params")
    try super.init(object:object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
      try encoder.encode(params, key: "params")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }

  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
}

// TODO: can be absorbed
public struct StorageAt: JSONCodable {
  public let address: Address
  public let key: String

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    address = try decoder.decode("address")
    key = try decoder.decode("key")
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(address, key: "address")
      try encoder.encode(key, key: "key")
    })
  }
}

public class GetStorageAtResponse: BurrowResponse {
  public let result: StorageItem

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}
