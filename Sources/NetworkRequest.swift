//
//  NetworkRequest.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 30/09/2016.
//  Copyright © 2016-17 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

import Foundation
import JSONCodable

// Network
public class GetNetworkInfoRequest: BurrowMessage {

  public let method = "\(BURROWDB).getNetworkInfo"

  public typealias Response = GetNetworkInfoResponse

  public override init(id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    super.init(id:id, callback: callback)
  }

  public required init(object: JSONObject) throws {
    try super.init(object:object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }

  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }

}

public class GetNetworkInfoResponse: BurrowResponse {

  public let result: NetworkInfoResult

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}

public struct NetworkInfoResult: JSONCodable {
  public let client_version: String
  public let moniker: String
  public let listening: Bool
  public let listeners: [String]
  public let peers: [Peer]

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    client_version = try decoder.decode("client_version")
    moniker = try decoder.decode("moniker")
    listening = try decoder.decode("listening")
    listeners = try decoder.decode("listeners")
    peers = try decoder.decode("peers")
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(client_version, key: "client_version")
      try encoder.encode(moniker, key: "moniker")
      try encoder.encode(listening, key: "listening")
      try encoder.encode(listeners, key: "listeners")
      try encoder.encode(peers, key: "peers")
    })
  }
}

public class GetClientVersionRequest: BurrowMessage {

  public let method = "\(BURROWDB).getClientVersion"

  public typealias Response = GetClientVersionResponse

  public override init(id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    super.init(id:id, callback: callback)
  }

  public required init(object: JSONObject) throws {
    try super.init(object:object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }

  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
}

public class GetClientVersionResponse: BurrowResponse {

  public let result: String

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result.client_version")
    try super.init(object: object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(["client_version": result] as [String: String], key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}



public class GetMonikerRequest: BurrowMessage {

  public let method = "\(BURROWDB).getMoniker"
  public typealias Response = GetMonikerResponse

  public override init(id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    super.init(id:id, callback: callback)
  }

  public required init(object: JSONObject) throws {
    try super.init(object:object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }

  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
}

public class GetMonikerResponse: BurrowResponse {
  public let result: String

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result.moniker")
    try super.init(object: object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(["moniker": result] as [String: String], key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}



public class IsListeningRequest: BurrowMessage {

  public let method = "\(BURROWDB).isListening"

  public typealias Response = IsListeningResponse

  public override init(id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    super.init(id:id, callback: callback)
  }

  public required init(object: JSONObject) throws {
    try super.init(object:object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }

  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
}

public class IsListeningResponse: BurrowResponse {

  public let result: Bool

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result.listening")
    try super.init(object: object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(["listening": result] as [String: Bool], key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}


public class GetPeersRequest: BurrowMessage {

  public let method = "\(BURROWDB).getPeers"

  public typealias Response = GetPeersResponse

  public override init(id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    super.init(id:id, callback: callback)
  }

  public required init(object: JSONObject) throws {
    try super.init(object:object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }

  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
}

public class GetPeersResponse: BurrowResponse {

  public let result: [Peer]

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}


public class GetPeerRequest:BurrowMessage {
  public let method = "\(BURROWDB).getPeer"
  public let params: Address  // TODO: can be absorbed

  public typealias Response = GetPeerResponse
  public init(id: RequestId, address: Address, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    self.params = address
    super.init(id:id, callback: callback)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    params = try decoder.decode("params.address")
    try super.init(object:object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
      try encoder.encode(["address": params] as [String: String], key: "params")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }

  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return callback(try Response(JSONString: JSONString))
  }
}

public class GetPeerResponse: BurrowResponse {

  public let result: Peer

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}



public struct Peer: JSONCodable {
  public let is_outbound: Bool
  public let moniker: String
  public let chain_id: RequestId
  public let version: String
  public let host: String
  public let p2p_port: Int
  public let rpc_port: Int

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    is_outbound = try decoder.decode("is_outbound")
    moniker = (try? decoder.decode("moniker") ?? "")!
    chain_id = (try? decoder.decode("chain_id") ?? "")!
    version = (try? decoder.decode("version") ?? "")!
    host = (try? decoder.decode("host") ?? "")!
    p2p_port = (try? decoder.decode("p2p_port") ?? 0)!
    rpc_port = (try? decoder.decode("rpc_port") ?? 0)!

  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(is_outbound, key: "is_outbound")
      try encoder.encode(moniker, key: "moniker")
      try encoder.encode(chain_id, key: "chain_id")
      try encoder.encode(version, key: "version")
      try encoder.encode(host, key: "host")
      try encoder.encode(p2p_port, key: "p2p_port")
      try encoder.encode(rpc_port, key: "rpc_port")
    })
  }
}
