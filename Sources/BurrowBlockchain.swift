//
//  BurrowBlockchain.swift
//  BurrowBlockchain

//
//  Created by Alex Tran-Qui on 11/07/16.
//  Copyright © 2016-17 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

import Foundation
import JSONCodable
import ErisKeys
#if !os(iOS)
    import Venice // coroutines for Linux or OSX
    import HTTPClient
#endif

#if os(iOS)
    import Starscream // Websockets for ios (multipthreaded)
#endif

public class BurrowBlockchain {
  
  
  var socket:  WebSocketWrapper
  
  var chain_id: String = ""
  let name: String
  public let key: ErisKey
  var sequence: Int = -1
  var _accountExists: Bool = false
  var debug: Bool
  var retry: Bool
    let url: URL
    let wsUrl: URL
  
  public var accountExists: Bool {
    get {
      return _accountExists
    }
  }
  
  public let reqGen = RequestIdGenerator(UUID().uuidString)
  
  // TODO: logging
  
  var openRequests =  [RequestId: BurrowMessage]()
  var openEvents = [Address: [String: (BurrowResponse) -> Void]]()
  
  var subscriptions = [String: String]() // unsubcription ids for open events
  
    public init(url: URL, wsUrl: URL, key: ErisKey, _ name: String = "", _ debug: Bool = false, _ retry: Bool = true) {
    self.url = url
    self.wsUrl = wsUrl
    self.key = key
    self.name = name
    self.debug = debug
    self.retry = retry
    #if os(iOS)
      self.socket = WebSocket(url: wsUrl)
    #else
      self.socket = ZewoWebSocket(url: wsUrl)
    #endif
    
    self.socket.onConnect = {
      self.onConnect()
    }
    
    self.socket.onDisconnect = { (error: NSError?) in
      self.onDisconnect(error: error)
    }
    
    self.socket.onText = { (text: String) in
      self.onText(text: text)
    }
    
    self.socket.onData = { (data: Data) in
      self.onData(data: data)
    }
    
    self.socket.onPong = { (data: Data?) in
      self.onPong(data: data)
    }
    
  }
  
  public func connect() {
    print("Blockchain \(name) is connecting...")
    socket.connect()
    socket.listenForResponse(isTrue: { return !self.isConnected() })
  }
  
  public func initializeWS() {
    // sets the chain id, the sequence number for the account we are using.
    // TODO: make thread safe!!!!
    self.send(msg: GetChainIdRequest(id: reqGen.next(), callback: { response in
      let r = response as! GetChainIdResponse
      self.chain_id = r.chain_id
      return response
    }))
    // TODO: make thread safe!!!!
    self.send(msg: GetAccountRequest(id: reqGen.next(), address: key.account, callback: { response in
      let r = response as! GetAccountResponse
      self.sequence = r.result.sequence
      self._accountExists = ( r.result.balance != 0) // use this as proxy for account existence
      return response
    }))
    //
    
  }

    public func initialize() {
        // sets the chain id, the sequence number for the account we are using.
        // TODO: make thread safe!!!!
        _ = self.HTTPSend(msg: GetChainIdRequest(id: reqGen.next(), callback: { response in
            let r = response as! GetChainIdResponse
            self.chain_id = r.chain_id
            return response
        }))
        // TODO: make thread safe!!!!
        _ = self.HTTPSend(msg: GetAccountRequest(id: reqGen.next(), address: key.account, callback: { response in
            let r = response as! GetAccountResponse
            self.sequence = r.result.sequence
            self._accountExists = ( r.result.balance != 0) // use this as proxy for account existence
            return response
        }))
        //
        
    }

    public var nextSeqWS: Int {
        get {
            socket.listenForResponse(isTrue: { return sequence == -1 }, timeout: 200)
            sequence += 1
            return sequence
        }
    }
    
    public var chainIdWS: String {
        get {
            socket.listenForResponse(isTrue: { return chain_id == "" }, timeout: 200)
            return chain_id
        }
    }
    
    public var nextSeq: Int {
        get {
            sequence += 1
            return sequence
        }
    }
    
    public var chainId: String {
        get {
            return chain_id
        }
    }
    
    public func HTTPSend(msg: BurrowMessage) -> BurrowResponse {
        var res = BurrowResponse(id: "")
        msg.sign({return self.nextSeq}, {return self.chainId}, key)
        do {
            if (debug) {
                print("[\(Date())] -H-\(name)-> : \(try msg.toJSONString())")
            }
            
            #if os(iOS)
                // TODO: use NSURLConnection
            #else
                // TODO: use Zewo
                let client = try Client(url: url)
                
                let r = try client.post("/rpc", headers: ["Content-type" : "application/json"], body: try msg.toJSONString())
                var b = r.body
                let response = try String(buffer: try b.becomeBuffer(deadline: now()))
            #endif
            
            if (debug) {
                print("[\(Date())] <-H-\(name)- : \(response)")
            }

            // callback
            res = try msg.parseResponse(JSONString: response)
            
        } catch let error {
            print("\(error)")
        }
        
        return res
    }
    
  public func send(msg: BurrowMessage){
    // argument evaluation is delayed such that nextSeq only moves the sequence forward if there is a signing happening.
    msg.sign({return self.nextSeq}, {return self.chainId}, key)
    do {
      if (debug) {
        print("[\(Date())] -W-\(name)-> : \(try msg.toJSONString())")
      }
      openRequests[msg.id] = msg
      
      try socket.write(string: msg.toJSONString())
    } catch let error {
      print(error)
    }
  }
  
  
  public func isConnected() -> Bool {
    return self.socket.isConnected
  }
  
  /// MARK: WebSocketDelegate
  private func onDisconnect(error: NSError?) {
    print("websocket is disconnected: \(String(describing: error?.localizedDescription))")
  }
  
  private func onConnect() {
    print("Blockchain \(name) is connected")
  }
  
  private func onData(data: Foundation.Data) {
    // TODO: process incoming messages
  }
  
  private func onText(text: String) {
    // based on the id of the inconming request, should deserialize
    // first deser with a simple requestId object and route using the return hash
    #if os(iOS)
      // TODO: this should be done on a different thread?
      process(text)
    #else
      co {
        self.process(text)
      }
    #endif
  }
  
  /// MARK: WebSocketPongDelegate
  private func onPong(data: Foundation.Data?) {
    socket.write(ping: Foundation.Data())
  }
  
  
  private func process(_ text: String) {
    let debug = self.debug
    let retry = self.retry
    do {
      if (debug) {
        print("[\(Date())] <-W-\(name)- : \(text)")
      }
      let res = try BurrowResponse(JSONString: text)
        
      if (res.error == nil) {
        
        _ = try self.openRequests[res.id]?.parseResponse(JSONString: text)
        
      } else {
        let err = res.error!
        
        switch err.message {
        case let x where x.hasPrefix("Error broadcasting transaction: Error invalid sequence. Got "):
          print("Sequence out of sync.")
          let a = x.components(separatedBy: .whitespaces)
          self.sequence = Int(a.last!)! - 1
          print("resetting sequence to \(self.sequence)")
          if (retry) {
            print("Retrying...")
            if let msg = self.openRequests[res.id] {
              self.send(msg: msg)
            }
          }
        default:
          break
        }
      }
    } catch let error {
        print(error)
    }
    // TODO: remove processed requests.
    // TODO: be careful about events they may lead to multiple responses tied to the same id
  }
}
