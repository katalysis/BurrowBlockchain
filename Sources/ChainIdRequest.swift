//
//  ChainIdRequest.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 30/09/2016.
//  Copyright © 2016-17 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

import Foundation
import JSONCodable

import Foundation


public class GetChainIdRequest: BurrowMessage {
    public let method = "\(BURROWDB).getChainId"
    
    public typealias Response = GetChainIdResponse
    
    public override init(id: RequestId, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
        super.init(id:id, callback:callback)
    }
    
    public required init(object: JSONObject) throws {
        try super.init(object:object)
    }
    
    public override func toJSON() throws -> Any {
        var res = try JSONEncoder.create({ (encoder) in
            try encoder.encode(method, key: "method")
        })
        for e in (try super.toJSON()) as! JSONObject {
            res[e.key] = e.value
        }
        return res
    }
    
    public override func parseResponse(JSONString: String) throws -> BurrowResponse {
        return callback(try Response(JSONString: JSONString))
    }
    
}

public class GetChainIdResponse: BurrowResponse {
    public let chain_id: String
    
    public required init(object: JSONObject) throws {
        let decoder = JSONDecoder(object: object)
        chain_id = try decoder.decode("result.chain_id")
        try super.init(object: object)
    }
    
    public override func toJSON() throws -> Any {
        var res = try JSONEncoder.create({ (encoder) in
            try encoder.encode(["chain_id": chain_id] as [String: String], key: "result")
        })
        for e in (try super.toJSON()) as! JSONObject {
            res[e.key] = e.value
        }
        return res
    }
}

