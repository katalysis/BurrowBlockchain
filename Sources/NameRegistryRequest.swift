//
//  NameRegistryRequest.swift
//  BurrowBlockchain
//
//  Created by Alex Tran-Qui on 30/09/2016.
//  Copyright © 2016-17 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

import Foundation
import JSONCodable

// Name Registry
public typealias GetNameRegEntriesParams = FiltersParams // expires(Int), owner(byte[]), name(string), data(string)

public class GetNameRegEntriesRequest: BurrowMessage {
  public let method = "\(BURROWDB).getNameRegEntries"
  public let params: GetNameRegEntriesParams

  public typealias Response = GetNameRegEntriesResponse
  public init(id: RequestId, params: GetNameRegEntriesParams, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    self.params = params
    super.init(id:id, callback: callback)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    params = try decoder.decode("params")
    try super.init(object:object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
      try encoder.encode(params, key: "params")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }

  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return try callback(Response(JSONString: JSONString))
  }
}

public class GetNameRegEntriesResponse: BurrowResponse {

  public let unknown: Int // TODO: check Eris Go code
  public let result: NameRegEntriesResult

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    unknown = try decoder.decode("result[0]")
    result = try decoder.decode("result[1]")
    try super.init(object: object)
  }

  public override func toJSON() throws -> Any {
    var res = JSONObject()
    var t = [Any]()
    t.append(try unknown.toJSON())
    t.append(try result.toJSON())
    res["result"] = t
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}

public struct NameRegEntriesResult: JSONCodable {
  public let block_height: BlockHeight
  public let names: [NameRegEntry]

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    block_height = try decoder.decode("block_height")
    names = try decoder.decode("names")
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(block_height, key: "block_height")
      try encoder.encode(names, key: "names")
    })
  }
}


public struct NameRegEntry: JSONCodable {
  public let owner: Address
  public let name: String
  public let data: String // TODO: should this be restricted somehow?
  public let expires: Int

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    owner = try decoder.decode("owner")
    name = try decoder.decode("name")
    data = try decoder.decode("data")
    expires = try decoder.decode("expires")
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(owner, key: "owner")
      try encoder.encode(name, key: "name")
      try encoder.encode(data, key: "data")
      try encoder.encode(expires, key: "expires")
    })
  }
}

public class GetNameRegEntryRequest: BurrowMessage {

  public let method = "\(BURROWDB).getNameRegEntry"
  public let params: String

  public typealias Response = GetNameRegEntryResponse
  public init(id: RequestId, name: String, callback: @escaping (BurrowResponse) -> BurrowResponse = {r in return r}) {
    self.params = name
    super.init(id:id, callback: callback)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    params = try decoder.decode("params.name")
    try super.init(object:object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(method, key: "method")
      try encoder.encode(["name": params] as [String: String], key: "params")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }

  public override func parseResponse(JSONString: String) throws -> BurrowResponse {
    return try callback(Response(JSONString: JSONString))
  }
}

public class GetNameRegEntryResponse: BurrowResponse {

  public let result: NameRegEntry

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    result = try decoder.decode("result")
    try super.init(object: object)
  }

  public override func toJSON() throws -> Any {
    var res = try JSONEncoder.create({ (encoder) in
      try encoder.encode(result, key: "result")
    })
    for e in (try super.toJSON()) as! JSONObject {
      res[e.key] = e.value
    }
    return res
  }
}
