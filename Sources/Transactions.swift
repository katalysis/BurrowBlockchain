//
//  Transactions.swift
//  BurrowBlockchain
//
//  Created by Alex Tran Qui on 11/07/16.
//  Copyright © 2016-17 Katalysis / Alex Tran Qui (alex.tranqui@gmail.com). All rights reserved.
//
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//
//

// https://github.com/eris-ltd/eris-db/blob/develop/docs/specs/api.md

import Foundation
import JSONCodable
import ErisKeys

// Signing: https://github.com/eris-ltd/eris-db/blob/develop/txs/tx.go

// "{\"chain_id\":\"simplechain\",\"tx\":[2,{\"address\":\"BB43DD0FBA5829EEAACE4724A478385FB54756CD\",\"data\":\"53703F5C0000000000000000000000000000000000000000000000000000000000000064\",\"fee\":0,\"gas_limit\":1000000,\"input\":{\"address\":\"17ED8B7ECF81D49743A880FFFDA0DDB020C1D62B\",\"amount\":1,\"sequence\":19}}]}"

/*
struct Signable {
  var chain_id: String
  var tx: [TransactionType, Tx]
}

 where Tx is a transaction but where the signature and pub_key are omited...
*/


// From a


public typealias BlockHash = String // 32 bytes as hext string
public typealias TxHash = String // 32 bytes as hext string


public enum TransactionDecodeError: Error {
  case wrongType(expected: TransactionType, got: TransactionType)
}

extension TransactionDecodeError: CustomStringConvertible {
  public var description: String {
    switch self {
    case .wrongType(let expected, let got):
      return String("Wrong Transaction Type. Expected: \(expected), got: \(got)")
    }
  }
}


public struct Signature {
  public let type: KeyType
  public let sig: String // 64 bytes hex string

  public init(_ sig: String, type: KeyType = .ed25519) {
    self.type = type
    self.sig = sig
  }
}

public struct TxInput: JSONCodable {
  public let address: Address
  public let amount: Int // amount is the quantity of "Ethers" send as part of the transaction
  fileprivate var _sequence: Int? // this is the sequence number of that transaction for the given address. sequence needs to be sequential numbers
  fileprivate var _signature: Signature?
  public let pub_key: PubKey?
  
  public var signature: Signature {
    get {
      return _signature ?? Signature("NOT SIGNED")
    }
  }
  
  public var sequence: Int {
    get {
      return _sequence ?? -1
    }
  }

  public init(from: Address, amount: Int, pubKey: String) {
    self.address = from
    self.amount = amount
    self.pub_key = PubKey(key: pubKey)
  }

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    amount = try decoder.decode("amount")
    _sequence = try decoder.decode("sequence")
    address = try decoder.decode("address")
    let sig: String = try decoder.decode("signature[1]")
    let type: KeyType = try decoder.decode("signature[0]")
    _signature = Signature(sig,type: type)
    do {
      let key: String = try decoder.decode("pub_key[1]")
      let keytype: KeyType = try decoder.decode("pub_key[0]")
      pub_key = PubKey(key: key, type: keytype)
    } catch {
      pub_key = nil
    }
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(amount, key: "amount")
      try encoder.encode(sequence, key: "sequence")
      try encoder.encode(address, key: "address")

      try encoder.encode([signature.type.rawValue, signature.sig] as [JSONEncodable], key: "signature")
      if (pub_key != nil) {
        try encoder.encode([pub_key!.type.rawValue, pub_key!.key] as [JSONEncodable], key: "pub_key")
      }
    })
  }
}

public struct TxOutput: JSONCodable { // TODO: not sure how this is used
  public let address: Address
  public let amount: Int

  public init(address: Address, amount: Int) {
    self.address = address
    self.amount = amount
  }

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    address = try decoder.decode("address")
    amount = try decoder.decode("amount")
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(address, key: "address")
      try encoder.encode(amount, key: "amount")
    })
  }
}

public struct BlockPart: JSONCodable { // TODO: not sure how this is used
  public let total: Int
  public let hash:  BlockHash

  public init(total: Int, hash: BlockHash) {
    self.total = total
    self.hash = hash
  }

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    total = try decoder.decode("total")
    hash = try decoder.decode("hash")
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(total, key: "total")
      try encoder.encode(hash, key: "hash")
    })
  }
}

public struct Vote: JSONCodable { // TODO: not sure how this is used
  public let height: BlockHeight
  public let type: Int
  public let block_hash: BlockHash
  public let block_parts: BlockPart
  public let signature: Signature

  public init(height: BlockHeight, type: Int, blockHash: BlockHash, blockParts: BlockPart, signature: Signature) {
    self.height = height
    self.type = type
    self.block_hash = blockHash
    self.block_parts = blockParts
    self.signature = signature
  }

  public init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    type = try decoder.decode("type")
    height = try decoder.decode("height")
    block_hash = try decoder.decode("block_hash")
    block_parts = try decoder.decode("block_parts")
    let sig: String = try decoder.decode("signature[1]")
    let sigtype: KeyType = try decoder.decode("signature[0]")
    signature = Signature(sig,type: sigtype)
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(type, key: "type")
      try encoder.encode(height, key: "height")
      try encoder.encode(block_hash, key: "block_hash")
      try encoder.encode(block_parts, key: "block_parts")
      try encoder.encode([signature.type.rawValue, signature.sig] as [JSONEncodable], key: "signature")
    })
  }
}

public enum TransactionType: Int {
  // Account transactions
  case TxTypeSend = 0x01
  case TxTypeCall = 0x02
  case TxTypeName = 0x03

  // Validation transactions
  case TxTypeBond    = 0x11
  case TxTypeUnbond  = 0x12
  case TxTypeRebond  = 0x13
  case TxTypeDupeout = 0x14

  // Admin transactions
  case TxTypePermissions = 0x20

}


public class Tx:JSONCodable { // TODO: check if this is still necessary or if this can be set to a protocol instead

  public let type: TransactionType

  public init(type: TransactionType) {
    self.type = type
  }

  public required init(object: JSONObject) throws {
    type = .TxTypeCall // TODO: hacked default, to remove
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(type, key: "type") // TODO: hacked default, to remove
    })
  }
  public func msgToSign(_ sequence: Int, _ chainID: String) -> String {
    return ""
  }

  public func sign(_ sequence: Int, _ chainId: String, _ key: ErisKey) {

  }

}

// Account transactions
public class SendTx: Tx {
  // TODO: not sure why there are many inputs there.
  var _inputs:  [TxInput] // TODO: not sure how this is used
  public let outputs: [TxOutput] // TODO: not sure how this is used

  public var inputs: [TxInput] {
    get {
      return _inputs
    }
  }
  
  public init(inputs: [TxInput], outputs: [TxOutput]) {
    self._inputs = inputs
    self.outputs = outputs
    super.init(type: .TxTypeSend)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    _inputs = try decoder.decode("inputs")
    outputs = try decoder.decode("outputs")
    super.init(type: .TxTypeSend)
  }

  public override func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(_inputs, key: "inputs")
      try encoder.encode(outputs, key: "outputs")
    })
  }

  public override func msgToSign(_ sequence: Int, _ chainID: String) -> String {
    // signing requires a specific order of the json string, hence need to manually deserialize
    var s = "{\"chain_id\":\"" + chainID + "\",\"tx\":[" + String(type.rawValue) + ",{\"inputs\":["
    var separator = ""
    for i in self.inputs {
      s += separator + "{\"address\":\"" + i.address + "\",\"amount\":" +  String(i.amount)
      s += ",\"sequence\":" + String(sequence) + "}"
      separator = ","
    }
    s += "],\"outputs\":["
    separator = ""
    for o in self.outputs {
      s += separator + "{\"address\":\"" + o.address + "\",\"amount\":" +  String(o.amount) + "}"
      separator = ","
    }
    s += "]}]}"
    return s
  }

  public override func sign(_ sequence: Int, _ chainId: String, _ key: ErisKey) {
    let data: [UInt8] = Array(self.msgToSign(sequence, chainId).utf8)
    for i in 0..<self.inputs.count {
      self._inputs[i]._sequence = sequence
      self._inputs[i]._signature = Signature((key.signAsStr(data)))
    }
  }

}

public class CallTx:  Tx {
  var _input: TxInput
  public let to: Address
  public let gas_limit: Int // TODO: not sure how this is used
  public let fee: Int // TODO: not sure how this is used
  public let data: BCData

  public var input: TxInput {
    get {
      return _input
    }
  }
  
  public init(input: TxInput, to: Address, gas_limit: Int, fee: Int, data: BCData) {
    self._input = input
    self.to = to
    self.gas_limit = gas_limit
    self.fee = fee
    self.data = data
    super.init(type: .TxTypeCall)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    _input = try decoder.decode("input")
    to = try decoder.decode("address")
    gas_limit = try decoder.decode("gas_limit")
    fee = try decoder.decode("fee")
    data = try decoder.decode("data")
    super.init(type: .TxTypeCall)
  }

  public override func sign(_ sequence: Int, _ chainId: String, _ key: ErisKey) {
    let data: [UInt8] = Array(self.msgToSign(sequence, chainId).utf8)
    self._input._sequence = sequence
    self._input._signature = Signature((key.signAsStr(data)))
  }

  public override func toJSON() throws -> Any {
  return try JSONEncoder.create({ (encoder) in
      try encoder.encode(input, key: "input")
      try encoder.encode(to, key: "address")
      try encoder.encode(gas_limit, key: "gas_limit")
      try encoder.encode(fee, key: "fee")
      try encoder.encode(data, key: "data")
    })
  }

  public override func msgToSign(_ sequence: Int, _ chainID: String) -> String {
    // signing requires a specific order of the json string, hence need to manually deserialize
    var s = "{\"chain_id\":\"" + chainID + "\",\"tx\":[" + String(type.rawValue) + ",{\"address\":\"" + to
    s += "\",\"data\":\"" + data + "\",\"fee\":" + String(fee) + ",\"gas_limit\":" + String(gas_limit)
    s += ",\"input\":{\"address\":\"" + input.address + "\",\"amount\":" +  String(input.amount)
    s += ",\"sequence\":" + String(sequence) + "}}]}"
    return s
  }
}

public class NameTx: Tx {
  var _input: TxInput
  public let name: String
  public let data: BCData
  //var amount: Int // TODO: not sure how this is used
  public let fee: Int // TODO: not sure how this is used
  
  public var input: TxInput {
    get {
      return _input
    }
  }

  public init(input: TxInput, name: String/*, amount: Int*/, fee: Int, data: BCData) {
    self._input = input
    self.name = name
    //self.amount = amount
    self.fee = fee
    self.data = data
    super.init(type: .TxTypeName)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    _input = try decoder.decode("input")
    name = try decoder.decode("name")
    //amount = try decoder.decode("amount")
    fee = try decoder.decode("fee")
    data = try decoder.decode("data")
    super.init(type: .TxTypeName)
  }

  public override func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(input, key: "input")
      try encoder.encode(name, key: "name")
      //try encoder.encode(amount, key: "amount")
      try encoder.encode(fee, key: "fee")
      try encoder.encode(data, key: "data")
    })
  }

  public override func msgToSign(_ sequence: Int, _ chainID: String) -> String {
    // signing requires a specific order of the json string, hence need to manually deserialize
    var s = "{\"chain_id\":\"" + chainID + "\",\"tx\":[" + String(type.rawValue) + ",{\"data\":\"" + data
    s += "\",\"fee\":" + String(fee)
    s += ",\"input\":{\"address\":\"" + input.address + "\",\"amount\":" +  String(input.amount)
    s += ",\"sequence\":" + String(sequence) + "},\"name\":\"" + name + "\"}]}"
    return s
  }
  public override func sign(_ sequence: Int, _ chainId: String, _ key: ErisKey) {
    let data: [UInt8] = Array(self.msgToSign(sequence, chainId).utf8)
    self._input._sequence = sequence
    self._input._signature = Signature((key.signAsStr(data)))
  }

}

// Validation transactions
// TODO: not sure how they are used
public class BondTx:  Tx {
  public let pub_key: PubKey // no type bytes here
  public let signature: Signature // no type bytes here
  var _inputs: [TxInput]
  public let unbond_to: [TxOutput]
  
  public var inputs: [TxInput] {
    get {
      return _inputs
    }
  }

  public init(pubKey: PubKey, signature: Signature, inputs: [TxInput], unbond_to: [TxOutput]) {
    self.pub_key = pubKey
    self.signature = signature
    self._inputs = inputs
    self.unbond_to = unbond_to
    super.init(type: .TxTypeBond)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    let key: String = try decoder.decode("pub_key")
    pub_key = PubKey(key: key, type: .ed25519)
    let sig: String = try decoder.decode("signature")
    signature = Signature(sig,type: .ed25519)
    _inputs = try decoder.decode("inputs")
    unbond_to = try decoder.decode("unbond_to")
    super.init(type: .TxTypeBond)
  }


  public override func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(signature.sig, key: "signature")
        try encoder.encode(pub_key.key, key: "pub_key")
      try encoder.encode(inputs, key: "inputs")
      try encoder.encode(unbond_to, key: "unbond_to")
    })
  }

  public override func msgToSign(_ sequence: Int, _ chainID: String) -> String {
    var s = ""
    s = ""
    return s
  }
  
  public override func sign(_ sequence: Int, _ chainId: String, _ key: ErisKey) {
    let data: [UInt8] = Array(self.msgToSign(sequence, chainId).utf8)
    for i in 0..<self.inputs.count {
      self._inputs[i]._sequence = sequence
      self._inputs[i]._signature = Signature((key.signAsStr(data)))
    }
  }
}

public class UnbondTx: Tx {
  public let address: Address
  public let height: BlockHeight
  public let signature: Signature

  public init(address: Address, height: BlockHeight, signature: Signature) {
    self.address = address
    self.height = height
    self.signature = signature
    super.init(type: .TxTypeUnbond)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    address = try decoder.decode("address")
    height = try decoder.decode("height")
    let sig: String = try decoder.decode("signature[1]")
    let sigtype: KeyType = try decoder.decode("signature[0]")
    signature = Signature(sig,type: sigtype)
    super.init(type: .TxTypeUnbond)
  }

  public override func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(address, key: "address")
      try encoder.encode(height, key: "height")
      try encoder.encode([signature.type.rawValue, signature.sig] as [JSONEncodable], key: "signature")
    })
  }

  public override func msgToSign(_ sequence: Int, _ chainID: String) -> String {
    var s = ""
    s = ""
    return s
  }
}

public class RebondTx: Tx {
  public let address: Address
  public let height: BlockHeight
  public let signature: Signature

  public init(address: Address, height: BlockHeight, signature: Signature) {
    self.address = address
    self.height = height
    self.signature = signature
    super.init(type: .TxTypeRebond)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    address = try decoder.decode("address")
    height = try decoder.decode("height")
    let sig: String = try decoder.decode("signature[1]")
    let sigtype: KeyType = try decoder.decode("signature[0]")
    signature = Signature(sig,type: sigtype)
    super.init(type: .TxTypeRebond)
  }

  public override func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(address, key: "address")
      try encoder.encode(height, key: "height")
      try encoder.encode([signature.type.rawValue, signature.sig] as [JSONEncodable], key: "signature")
    })
  }

  public override func msgToSign(_ sequence: Int, _ chainID: String) -> String {
    var s = ""
    s = ""
    return s
  }
}

public class DupeoutTx: Tx {
  public let address: Address
  public let vote_a: Vote
  public let vote_b: Vote

  public init(address: Address, voteA: Vote, voteB: Vote) {
    self.address = address
    self.vote_a = voteA
    self.vote_b = voteB
    super.init(type: .TxTypeDupeout)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    address = try decoder.decode("address")
    vote_a = try decoder.decode("vote_a")
    vote_b = try decoder.decode("vote_b")
    super.init(type: .TxTypeDupeout)
  }

  public override func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(address, key: "address")
      try encoder.encode(vote_a, key: "vote_a")
      try encoder.encode(vote_b, key: "vote_b")
    })
  }

}

// Admin transactions
// TODO: not sure how this is used
public class PermissionTx: Tx {
  var _input: TxInput
  public let args: PermArgs
  
  public var input: TxInput {
    get {
      return _input
    }
  }

  public init(input: TxInput, args: PermArgs) {
    self._input = input
    self.args = args
    super.init(type: .TxTypePermissions)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    _input = try decoder.decode("input")
    let args_type: PermArgsType = try decoder.decode("args[0]")
    switch args_type {
    case .hasBase:
      let a: HasBaseArgs = try decoder.decode("args[1]")
      args = a
    case .setBase:
      let a: SetBaseArgs = try decoder.decode("args[1]")
      args = a
    case .unsetBase:
      let a: UnsetBaseArgs = try decoder.decode("args[1]")
      args = a
    case .setGlobal:
      let a: SetGlobalArgs = try decoder.decode("args[1]")
      args = a
    case .hasRole:
      let a: HasRoleArgs = try decoder.decode("args[1]")
      args = a
    case .addRole:
      let a: AddRoleArgs = try decoder.decode("args[1]")
      args = a
    case .rmRole:
      let a: RmRoleArgs = try decoder.decode("args[1]")
      args = a
    }
    super.init(type: .TxTypePermissions)
  }

  public override func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(input, key: "input")
      try encoder.encode([args.type.rawValue, args] as [JSONEncodable], key: "args")

    })

  }
  
  public override func msgToSign(_ sequence: Int, _ chainID: String) -> String {
    // signing requires a specific order of the json string, hence need to manually deserialize
    var s = "{\"chain_id\":\"" + chainID + "\",\"tx\":[" + String(type.rawValue) + ",{\"args\":\"[" + String(args.type.rawValue) + ","
    switch args.type {
    case .hasBase:
      let a = args as! HasBaseArgs
      s += "{\"address\":\"" + a.address + "\",\"permission\":" + String(a.permission) + "}"
    case .unsetBase:
      let a = args as! UnsetBaseArgs
      s += "{\"address\":\"" + a.address + "\",\"permission\":" + String(a.permission) + "}"
    case .setBase:
      let a = args as! SetBaseArgs
      s += "{\"address\":\"" + a.address + "\",\"permission\":" + String(a.permission) + ",\"value\":" + String(a.value) + "}"
    case .setGlobal:
      let a = args as! SetGlobalArgs
      s += "{\"permission\":" + String(a.permission) + ",\"value\":" + String(a.value) + "}"
    case .hasRole:
      let a = args as! HasRoleArgs
      s += "{\"address\":\"" + a.address + "\",\"role\":\"" + a.role + "\"}"
    case .addRole:
      let a = args as! AddRoleArgs
      s += "{\"address\":\"" + a.address + "\",\"role\":\"" + a.role + "\"}"
    case .rmRole:
      let a = args as! RmRoleArgs
      s += "{\"address\":\"" + a.address + "\",\"role\":\"" + a.role + "\"}"
    }
    s += "]\",\"input\":{\"address\":\"" + input.address + "\",\"amount\":" +  String(input.amount)
    s += ",\"sequence\":" + String(sequence) + "}}]}"
    return s
  }

  public override func sign(_ sequence: Int, _ chainId: String, _ key: ErisKey) {
    let data: [UInt8] = Array(self.msgToSign(sequence, chainId).utf8)
    self._input._sequence = sequence
    self._input._signature = Signature((key.signAsStr(data)))
  }
}




public enum PermArgsType: Int {

  case hasBase = 0x01 // {address: "", permission: 12345}
  case setBase = 0x02 // {address: "", permission: 12345, value: true}
  case unsetBase = 0x03 // {address: "", permission: 12345}
  case setGlobal = 0x04 // {permission: 12345, value: true}
  case hasRole = 0x05 // {address: "", role: "toto"}
  case addRole = 0x06 // {address: "", role: "toto"}
  case rmRole = 0x07 // {address: "", role: "toto"}
}


public class PermArgs: JSONCodable {
  public let type: PermArgsType

  public init(type: PermArgsType) {
    self.type = type
  }

  public required init(object: JSONObject) throws {
    type = .hasBase // TODO: hacked default, to remove
  }

  public func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(type, key: "type") // TODO: hacked default, to remove
    })
  }

}

public class HasBaseArgs: PermArgs {
  public let address: Address
  public let permission: Int

  public init(address: Address, perm: Int) {
    self.address = address
    self.permission = perm
    super.init(type: .hasBase)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    address = try decoder.decode("address")
    permission = try decoder.decode("permission")
    super.init(type: .hasBase)
  }

  public override func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(address, key: "address")
      try encoder.encode(permission, key: "permission")
    })
  }
}

public class SetBaseArgs: PermArgs {
  public let address: Address
  public let value: Bool
  public let permission: Int

  public init(address: Address,perm: Int, value: Bool) {
    self.address = address
    self.value = value
    self.permission = perm
    super.init(type: .setBase)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    address = try decoder.decode("address")
    value = try decoder.decode("value")
    permission = try decoder.decode("permission")
    super.init(type: .setBase)
  }

  public override func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(address, key: "address")
      try encoder.encode(value, key: "value")
      try encoder.encode(permission, key: "permission")
    })
  }
}

public class UnsetBaseArgs: PermArgs {
  public let address: Address
  public let permission: Int

  public init(address: Address, perm: Int) {
    self.address = address
    self.permission = perm
    super.init(type: .unsetBase)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    address = try decoder.decode("address")
    permission = try decoder.decode("permission")
    super.init(type: .unsetBase)
  }

  public override func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(address, key: "address")
      try encoder.encode(permission, key: "permission")
    })
  }
}

public class SetGlobalArgs: PermArgs {
  public let value: Bool
  public let permission: Int

  public init(perm: Int, value: Bool) {
    self.value = value
    self.permission = perm
    super.init(type: .setGlobal)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    value = try decoder.decode("value")
    permission = try decoder.decode("permission")
    super.init(type: .setGlobal)
  }

  public override func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(value, key: "value")
      try encoder.encode(permission, key: "permission")
    })
  }
}

public class HasRoleArgs: PermArgs {
  public let address: Address
  public let role: String

  public init(address: Address, role: String) {
    self.address = address
    self.role = role
    super.init(type: .hasRole)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    address = try decoder.decode("address")
    role = try decoder.decode("role")
    super.init(type: .hasRole)
  }

  public override func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(address, key: "address")
      try encoder.encode(role, key: "role")
    })
  }

}

public class AddRoleArgs: PermArgs {
  public let address: Address
  public let role: String

  public init(address: Address, role: String) {
    self.address = address
    self.role = role
    super.init(type: .addRole)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    address = try decoder.decode("address")
    role = try decoder.decode("role")
    super.init(type: .addRole)
  }

  public override func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(address, key: "address")
      try encoder.encode(role, key: "role")
    })
  }
}

public class RmRoleArgs: PermArgs {
  public let address: Address
  public let role: String

  public init(address: Address, role: String) {
    self.address = address
    self.role = role
    super.init(type: .rmRole)
  }

  public required init(object: JSONObject) throws {
    let decoder = JSONDecoder(object: object)
    address = try decoder.decode("address")
    role = try decoder.decode("role")
    super.init(type: .rmRole)
  }

  public override func toJSON() throws -> Any {
    return try JSONEncoder.create({ (encoder) in
      try encoder.encode(address, key: "address")
      try encoder.encode(role, key: "role")
    })
  }
}
